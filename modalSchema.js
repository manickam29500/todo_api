const mongoose = require('mongoose');

const dbSchema = mongoose.Schema(
    {
        task: {
            type:String,
            required: [true,'this field is required']
        },
        date: {
            type:String,
            required: [true,'this field is required']
        },
        category: {
            type:Number,
            required: [true,'this field is required']
        },
        priority: {
            type:Number,
            required: [true,'this field is required']
        },
        completed: {
            type:Boolean,
            required: [true,'this field is required']
        }
    }
)

const schema = mongoose.model('schema',dbSchema);
module.exports = schema;